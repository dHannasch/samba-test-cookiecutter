import importlib
import io
import itertools
import os
import pathlib
import socket

import smbclient  # remember really from smbprotocol
import smbclient.path  # must be specifically imported
import smbclient._pool
import smbprotocol
import smbprotocol.dfs
import smbprotocol.exceptions
import smbprotocol.tree
import yaml

import test_samba_write


def autodetect_path_kind(path):
  if '\\' in path:
    return pathlib.PureWindowsPath(path)
  return pathlib.Path(path)


def test_access(DFS_initially_unavailable_but_exists_after, magic_path_that_makes_DFS_work, paths_found_in_listdir_but_maybe_not_covered, paths_that_should_exist_immediately):
  # for path in default_context['paths_that_should_exist']:
  #   try:
  #     assert smbclient.path.exists(path)
  #   except Exception as ex:
  #     raise ValueError(path) from ex
  # smbprotocol.exceptions.SMBOSError: [Error 0] Unknown NtStatus error returned 'STATUS_PATH_NOT_COVERED'
  # it seems maybe paths you can see, but not access, you cannot call smbclient.path.exists on
  for path in DFS_initially_unavailable_but_exists_after:
    expect_DFS_unavailable(path)
    expect_DFS_unavailable(path)
  if magic_path_that_makes_DFS_work:
    try:
      smbclient.path.exists(magic_path_that_makes_DFS_work)
    except smbprotocol.exceptions.SMBOSError as ex:
      assert 'STATUS_PATH_NOT_COVERED' in str(ex)
    else:
      assert False
    smbclient.listdir(magic_path_that_makes_DFS_work)
    assert smbclient.path.exists(magic_path_that_makes_DFS_work)
    for path in DFS_initially_unavailable_but_exists_after:
      smbclient.listdir(path)
  for path in paths_found_in_listdir_but_maybe_not_covered:
    pathObj: pathlib.Path = autodetect_path_kind(path)
    parent: pathlib.Path = pathObj.parent
    assert smbclient.path.exists(parent)
    assert smbclient.path.isdir(parent)
    if pathObj.name not in smbclient.listdir(parent):
      raise Exception(smbclient.listdir(parent), path)
    try:
      childExists = smbclient.path.exists(str(path))
    except smbprotocol.exceptions.PathNotCovered as ex:
      assert 'The contacted server does not support the indicated part of the DFS namespace.' in str(ex)
      assert 'STATUS_PATH_NOT_COVERED' in str(ex)
    except smbprotocol.exceptions.SMBOSError as ex:
      assert 'STATUS_PATH_NOT_COVERED' in str(ex)
    else:
      assert childExists
  for path in paths_that_should_exist_immediately:
    assert smbclient.path.exists(path)


def test_access_kwargs(default_context):
  return test_access(default_context['DFS_initially_unavailable_but_exists_after'], default_context['magic_path_that_makes_DFS_work'], default_context['paths_found_in_listdir_but_maybe_not_covered'], default_context['paths_that_should_exist_immediately'])


def expect_DFS_unavailable(path):
  try:
    smbclient.listdir(str(path))
  except smbprotocol.exceptions.DfsUnavailable as ex:
    assert 'DFS is unavailable on the contacted server.' in str(ex)
    assert 'STATUS_DFS_UNAVAILABLE' in str(ex)
    assert isinstance(ex.__context__, smbprotocol.exceptions.PathNotCovered)
    assert 'The contacted server does not support the indicated part of the DFS namespace.' in str(ex.__context__)
    assert 'STATUS_PATH_NOT_COVERED' in str(ex.__context__)
  else:
    assert False
  try:
    smbclient.path.exists(str(path))
  except smbprotocol.exceptions.PathNotCovered as ex:
    assert 'The contacted server does not support the indicated part of the DFS namespace.' in str(ex)
    assert 'STATUS_PATH_NOT_COVERED' in str(ex)
  except smbprotocol.exceptions.SMBOSError as ex:
    assert 'STATUS_PATH_NOT_COVERED' in str(ex)
  else:
    assert False


def expect_path_not_covered(default_context):
  try:
    test_samba_write.test_write(default_context)
  except smbprotocol.exceptions.PathNotCovered as ex:
    assert "Received unexpected status from the server: The contacted server does not support the indicated part of the DFS namespace." in str(ex)
    assert "STATUS_PATH_NOT_COVERED" in str(ex)
  except smbprotocol.exceptions.SMBOSError as ex:
    assert "Unknown NtStatus error returned 'STATUS_PATH_NOT_COVERED'" in str(ex)
  else:
    print("Often we get STATUS_PATH_NOT_COVERED when going for a path in another region, but not this time, apparently. Maybe by luck we got a server that serves both regions?")


def register_session(server_name: str, real_server_name: str, share_name: str, username: str, username_suffix: str, password: str):
  # https://github.com/jborean93/smbprotocol/blob/92ed80ba4681edc7a7c40eb8fefc1c66a417f064/smbprotocol/transport.py#L52
  print(f'socket.create_connection( ({server_name}, 445) ) =', socket.create_connection( (server_name, 445) ))
  full_username = username + username_suffix
  client_config = smbclient.ClientConfig(username=full_username, password=password)
  assert not client_config.lookup_domain(server_name)
  dfs_path = server_name
  p = smbclient._pool.get_smb_tree('\\\\' + dfs_path + '\\IPC$')
  ipc_tree: smbprotocol.tree.TreeConnect = p[0]
  file_path = p[1]
  assert not file_path
  DFSresponse: smbprotocol.dfs.DFSReferralResponse = smbclient._pool.dfs_request(ipc_tree, '\\\\' + dfs_path + '\\' + share_name)
  client_config.cache_referral(DFSresponse)
  referEntry: smbprotocol.dfs.ReferralEntry = client_config.lookup_referral((dfs_path, share_name))
  if referEntry.dfs_path != '\\\\' + dfs_path + '\\' + share_name:
    raise Exception('\\\\' + dfs_path + '\\' + share_name, referEntry.dfs_path)
  if referEntry.target_hint.target_path != '\\' + real_server_name + '\\' + share_name:
    raise Exception(referEntry.target_hint.target_path)

  return smbclient.register_session(server_name, username=full_username, password=password)


if __name__ == "__main__":
  default_context = yaml.safe_load(open('replay.yaml'))['cookiecutter']
  # https://github.com/jborean93/smbprotocol/blob/92ed80ba4681edc7a7c40eb8fefc1c66a417f064/smbprotocol/transport.py#L52
  session = register_session(default_context['server_name'], default_context['real_server_name'], default_context['share_name'], os.getenv('USERNAME'), default_context['username_suffix'], os.getenv('PASSWORD'))
  print(type(session))

  test_access_kwargs(default_context)
  expect_path_not_covered(default_context)
  session.disconnect()
  del session
  smbclient.reset_connection_cache()
  smbclient.delete_session(default_context['server_name'])
  importlib.reload(smbclient)
  importlib.reload(smbclient.path)
  importlib.reload(smbprotocol)
  expect_path_not_covered(default_context)

