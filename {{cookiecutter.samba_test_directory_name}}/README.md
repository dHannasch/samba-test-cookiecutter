
You could, of course, simply `mount` a Samba drive on the machine hosting the GitLab-Runner, inside a directory which was itself mounted into the GitLab-Runner. But then access to the drive would only be controlled by access to the GitLab-Runner; anyone able to run a job on the GitLab-Runner would be able to read the entire contents of the drive.

