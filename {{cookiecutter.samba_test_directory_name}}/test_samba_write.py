import io
import itertools
import os
import pathlib

import smbclient  # remember really from smbprotocol
import smbclient.path  # must be specifically imported
import smbprotocol.exceptions
import yaml


def autodetect_path_kind(path):
  if '\\' in path:
    return pathlib.PureWindowsPath(path)
  return pathlib.Path(path)


def test_write(default_context):
  if default_context['path_to_create_file']:
    path_to_create_file = autodetect_path_kind(default_context['path_to_create_file'])
    # Something about smbclient.listdir is magic
    try:
      smbclient.path.exists(str(path_to_create_file.parent))
    except smbprotocol.exceptions.SMBOSError as ex:
      assert "Unknown NtStatus error returned 'STATUS_PATH_NOT_COVERED'" in str(ex)
    else:
      print("smbclient.path.exists(str(path_to_create_file.parent)) unexpectedly succeeded!")
    smbclient.listdir(str(path_to_create_file.parent))
    if not smbclient.path.exists(str(path_to_create_file.parent)):
      # smbclient.makedirs can fail with smbprotocol.exceptions.SMBOSError: Unknown NtStatus error returned 'STATUS_ACCESS_DENIED' even if the directory already exists.
      smbclient.makedirs(str(path_to_create_file.parent), exist_ok=True)
    if smbclient.path.exists(str(path_to_create_file)):
      smbclient.remove(path_to_create_file)
    assert not smbclient.path.exists(str(path_to_create_file))
    with smbclient.open_file(str(path_to_create_file), 'wb') as outfile:
      assert isinstance(outfile, io.BufferedWriter)
      content = b"a" * (outfile.raw.fd.connection.max_write_size + 1024)
      # bytes_written = outfile.writeall(content)
      bytes_written = outfile.write(content)
      assert bytes_written == len(content)
    assert smbclient.path.exists(str(path_to_create_file))
    assert smbclient.stat(str(path_to_create_file)).st_size == len(content)
    with smbclient.open_file(str(path_to_create_file), mode='rb') as infile:
      assert infile.read() == content
    smbclient.remove(str(path_to_create_file))
    assert not smbclient.path.exists(str(path_to_create_file))


if __name__ == "__main__":
  default_context = yaml.safe_load(open('replay.yaml'))['cookiecutter']
  full_username = os.getenv('USERNAME') + default_context['username_suffix']
  smbclient.ClientConfig(username=full_username, password=os.getenv('PASSWORD'))
  session = smbclient.register_session(default_context['server_name'], username=full_username, password=os.getenv('PASSWORD'))
  test_write(default_context)

